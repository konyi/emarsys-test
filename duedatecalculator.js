"use strict";

// constants
const minMs=60*1000;
const hourMs=60*minMs;
const dayMs = 24*hourMs;

const startWorkMs=  9*hourMs;
const endWorkMs= 17*hourMs;
const workdayLenMs= 8*hourMs;

/**
	* @desc DueDateCalculator constructor
	* @param bool debug - debuggin enabled (default false)
*/
function DueDateCalculator(debug) {
	this.debug=debug || false;
}

/**
	* @desc Checking date is workday
	* @param Date dateobj - a date object to check
	* @return bool - true if date is workday
*/
DueDateCalculator.prototype.workDay=function(dateObj)
{
	var d=dateObj.getDay();
	return ((d>0) && (d<6));	//all days except Sat,Sun 
}

/**
	* @desc Checking date is in worktime
	* @param Date dateobj - a date object to check
	* @return bool - true if date is in workDay 9AM-5PM
*/
DueDateCalculator.prototype.workTime=function(dateObj)
{
	var d=dateObj.getHours();
	return (this.workDay(dateObj) && (d>=9) && (d<17)); // 9:00:00.000 - 16:59:59.999
}

/**
	* @desc print out a date object to console
	* @param string name - name prefix 
	* @param Date obj - a date object to print
*/
DueDateCalculator.prototype.logDateObj=function(name,dateMsObj)
{
	var d=new Date(dateMsObj);
	console.log(name+'\t',d.toLocaleString(),'|',d.toString());
}

/**
	* @desc Extract only date part from date object
	* @param Date dateobj - a date object to convert
	* @return Date - date only with timezone correction
*/
DueDateCalculator.prototype.getDateOf=function(dateObj)
{
	return (Math.floor(dateObj / dayMs)*dayMs) + (dateObj.getTimezoneOffset()*60000);
}

/**
	* @desc Calculate due date (only in M-F 9-17)
	* @param Date submitDate - a date object to convert
	* @return Number turnaroundHours - hours to add to submitDate
*/
DueDateCalculator.prototype.calculateDueDate=function(submitDate, turnaroundHours)
{
	var calculatedDatetime=null;

	if (!(submitDate instanceof Date)) {
		if (this.debug)
			console.error("submitDate is not a date");
		return null;
	}

	if (!this.workTime(submitDate)) {
		if (this.debug)
			console.error("submitDate is not in worktime:",submitDate);
		return null;
	}

	if (typeof turnaroundHours!='number') {
		if (this.debug)
			console.error("turnaroundTime is not a number");
		return null;
	}

	if (turnaroundHours<=0) {
		if (this.debug)
			console.error("turnaroundTime must be positive");
		return null;
	}
	// a fentiek helyett lehetne Error-t dobni ha az osztalyon kivul erdekel a hiba


	var rem = turnaroundHours*hourMs; //remaining time in msec
	var act=submitDate.getTime();		//submit in msec

	if (this.debug) {
		this.logDateObj('submitDate',submitDate);
		console.log('all\t\t',rem/hourMs,'hour;',rem/minMs,'mins')
		console.log('------------------------------------');
	}

	while (rem>0) {
		if (!this.workDay( new Date(act) )) {	// if not workday then go to next day
			act+=dayMs;
			continue;
		}

		var today=this.getDateOf(new Date(act)); // get today midnight
		var todayMs=(today+endWorkMs)-act;	//remaining time to end of workday

		if (rem<=todayMs) {
			calculatedDatetime=new Date(act+rem);	//got result
			rem=0;	//
			break;
		}
		else {
			rem-=todayMs;		// desc remaining with 1 day
			act=today+dayMs+startWorkMs;	//set act to work start of tomorrow morning
		}

		if (this.debug) {
			console.log('-'+todayMs/minMs,'=',rem/minMs)
			this.logDateObj('sow         ',today+startWorkMs );
			this.logDateObj('eow         ',today+endWorkMs );
			this.logDateObj('next         ',act);
		}
	}

	if (this.debug)
		this.logDateObj('return\t',calculatedDatetime)
	
	return calculatedDatetime;
}

module.exports = DueDateCalculator;