var calc=require('../duedatecalculator');

var testCases = [
	{dt: new Date(2009,02,04,10,35,0,0), ta:   4, check: new Date(2009,02,04,14,35,0,0),name:'intraday'},
	{dt: new Date(2009,02,04,16,30,0,0), ta:   6, check: new Date(2009,02,05,14,30,0,0),name:'overlapping'},
	{dt: new Date(2009,02,04,16,30,0,0), ta: 6.5, check: new Date(2009,02,05,15,00,0,0),name:'overlapping float'},
	{dt: new Date(2009,02,04,16,30,0,0), ta:81.5, check: new Date(2009,02,19,10,00,0,0),name:'multiple weeks'},
	{dt: new Date(2000,01,28,12,00,0,0), ta:  16, check: new Date(2000,02,01,12,0,0,0) ,name:'leapyear'},
	{dt: new Date(2001,01,28,12,00,0,0), ta:  16, check: new Date(2001,02,02,12,0,0,0) ,name:'not leapyear'},
	{dt: new Date(2001,11,31,16,00,0,0), ta:   2, check: new Date(2002, 0,01,10,0,0,0) ,name:'happy new year'},	
]

var testCasesError = [
	{dt: new Date(2009,02,04,16,30,0,0), ta:  -2, check: null	,name:'negative turnaround'},
	{dt: new Date(2016,09,15,10,00,0,0), ta:   1, check: null	,name:'invalid submit day (weekend)'},
	{dt: new Date(2009,02,04, 4,00,0,0), ta:  10, check: null	,name:'invalid submit time'},
	{dt: 123	, ta:  10, check: null 												 	,name:'submitDate number'},
	{dt: "123", ta:  10, check: null 												 	,name:'submitDate string'},
	{dt: "123", ta:"10", check: null 													,name:'ta string'},
	{check: null 												 											,name:'undefined inputs'},
	{dt: new Date(2001,11,31,16,00,0,0), ta:   0, check: null ,name:'zero turnaround'},
];


var allTest = testCases.concat(testCasesError);
/**
	* @desc testCase
	* @param Date date - submit date 
	* @param Number turnaround - turnaround time in hours
	* @param Date check - correct answer
	* @return bool - result is equal with check
*/
function testIt(date,turnaround,check) {
	var obj=new calc();
	var result=obj.calculateDueDate(date,turnaround);
	
	// console.log(check,result);
	if (result==null) {
		return (check==result);
	}
	else {
		if (check && result) {
			// console.log(result.toLocaleString(),check.toLocaleString())
			return (result.getTime()===check.getTime());
		}
		else
			return false;
	}
}

/**
	*	@desc running all tests
	* @param array of objects - test cases array of test objects objects
	* @return bool - all tests passed
*/
function runAllTests(cases,verbose) {
	var testsPassed=0;
	cases.forEach((item,idx)=>{
		var tres=testIt(item.dt, item.ta, item.check);
		console.log('Test#',idx+1+':',tres?'PASSED':'FAIL',(verbose)?'-> '+item.name:'');
		if (tres)
			testsPassed++;
	});
	console.log(`${testsPassed} of ${cases.length} tests passed\n`);
	return (testsPassed==cases.length);
}


var verbose=true;
// runAllTests(testCases,verbose);
// runAllTests(testCasesError,verbose);

runAllTests(allTest,verbose);